---
date: 2016-06-16T18:22:12-03:00
description: Aprenda como criar um script para enviar emails para uma lista de endereços com anexos personalizados
draft: true
tags:
- produtividade
- tutorial
- applescript
title: Como enviar emails no Outlook e Apple Mail usando AppleScript
---

set people2Send to {} -- start off with an empty list

    --set the path to your plain text file with addresses, and name, tab delimited
    --change the username

    set everybody to read file ("Macintosh HD:Users:sjardim:people_to_send.txt")
    --set the path to the pdf files. The TXT file MUST be saved in Mac OS Roman encoding 
    set rootPath to "/Users/sjardim/pdf/"

    set pcount to count paragraphs in everybody
    repeat with i from 1 to number of paragraphs in everybody
        set this_item to paragraph i of everybody
        
        set thisName to "" -- set to blank
        -- using try so if there is a an error, in this case it will be when the is no name, the script will carry on
        -- instead of stopping. The Name_text will reamin as "" if it does
        try
            --use delimiters (tab) in this case to split the result of this_item and try to set Name_text to the name
            set AppleScript's text item delimiters to tab
            set para_text to text of this_item
            --get the second half of paragraph 
            set thisName to text item 1 of para_text
        end try
        --get the first half of paragraph 
        set emailAddress to text item 2 of para_text
        set emailCcAddress to text item 3 of para_text
        set thisFile to rootPath & text item 4 of para_text & ".pdf"
        log thisFile
        
        if emailCcAddress as string is not equal to "" then
            copy {_name:thisName, _address:emailAddress, _Ccaddress:emailCcAddress, _file:thisFile} to end of people2Send
        else
            copy {_name:thisName, _address:emailAddress, _Ccaddress:"", _file:thisFile} to end of people2Send
        end if
        
        --reset the delimiters
        set AppleScript's text item delimiters to ""
        
    end repeat

    tell application "Microsoft Outlook"
        repeat with eachPerson in people2Send -- now loop through the names/addresses from above
            set theAccount to (second «class Eact»)
            set msgSubject to "Your Workshop Certificate"
            set fromPerson to "<strong>John Smith</strong>"
            set msgBody to "Dear " & (_name of eachPerson) & "," & "<br>" & "Thank you for attending our workshop. Please find enclosed your certificate of attendance as a PDF attachment." & "<br>" & "We thank you for taking part in this training initiative." & "<br><br>" & "Best regards," & "<br>" & fromPerson -- here's where you craft the message text
            set newMsg to (make new «class outm» with properties {«class cAct»:theAccount, «class subj»:msgSubject, «class ctnt»:msgBody}) -- create the message
            tell newMsg
                make new «class rcpt» at end of every «class trcp» with properties {«class emad»:{name:eachPerson's _name, «class radd»:eachPerson's _address}} -- add the recipient
                if eachPerson's _Ccaddress as string is not equal to "" then
                    make new «class rcpt» at end of every «class crcp» with properties {«class emad»:{«class radd»:eachPerson's _Ccaddress}} -- add the recipient
                end if
                make new «class cAtc» with properties {file:eachPerson's _file} -- add an attachment
            end tell
            open newMsg -- and just open it
            -- send newMsg -- and send it on its way
        end repeat
    end tell
