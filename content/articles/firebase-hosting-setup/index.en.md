---
date: 2017-06-02
title: Continuous deployment with Hugo, Bitbucket, and Firebase
description: How to host a Hugo static website with SSL and CDN support, for free!
featured_image: firebase.png
lang: en
category: development
tags:
- produtividade
- tutorial
- hugo
---

What a great surprise it was when I discovered that you can host a static website or app on [Firebase](https://firebase.google.com) extremely easy and, even on the free plan, you have a custom domain! :)

What the quick video below from their site:

{{< youtube jsRVHeQd5kU >}}

Além de banco de dados realtime e outras fantásticas funcionalidades para aplicações mobile e web, o Firebase, que foi [comprado pelo Google em 2014](https://techcrunch.com/2014/10/21/google-acquires-firebase-to-help-developers-build-better-realtime-apps/) também fornece um serviço de hosting para arquivos estáticos que todos *goodies* que sites modernos precisam:

- HTTPS automático;
- CDN com servidores no mundo inteiro;
- Deploy rápido através da Firebase CLI e;
- One-click rollbacks!

Acesse a [documentação de hospedagem no Firebase](https://firebase.google.com/docs/hosting/) para mais detalhes.


|                             | Firebase | Netlify | Amazon S3/Cloudfront |
|-----------------------------|----------|---------|----------------------|
| Dificuldade de Configuração | Baixa    | Baixa   | Média/Alta           |
| Complexidade de Manutenção  | Baixa    | Baixa   | Baixa                |

{{< figure src="/img/posts/2016/firebase/screenshot_do.png" title="Configurando os registros TXT na DigitalOcean" >}}

## Ativar seu dominio

Agora você precisa apontar os registros de DNS do seu domínio para o nosso Firebase. Os registros específicos A ou CNAME DNS são exibidos no Console Firebase *uma vez certificado do seu domínio é ativado*. 

Uma vez que as alterações de DNS têm propagado, os usuários serão capazes de acessar seu site de hospedagem Firebase sobre o domínio conectado.

Dúvidas, consulte a [documentação oficial](https://firebase.google.com/docs/hosting/custom-domain). 

