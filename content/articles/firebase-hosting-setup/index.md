---
date: 2017-06-02
title: Continuous deployment com Hugo, Bitbucket e Firebase
description: Como hospedar gratuitamente seu site em Hugo com direito a SSL e uma CDN
featured_image: firebase.png
category: development
tags:
- produtividade
- tutorial
- hugo
---

Neste tutorial, vamos usar o [Hugo](https://gohugo.io), o [Bitbucket](https://bitbucket.org) e o [Firebase](https://firebase.google.com) para construir, publicar e hospedar um site estático. 

Eu irei considerar que você sabe usar os comandos básico do *git* para controle de versão e tem uma conta do Bitbucket.

## Sobre o Firebase

Que excelente supresa tive ao descobrir que hospedar um site estático no [Firebase](https://firebase.google.com) é extremamente fácil, e mesmo no plano gratuito, você pode configurar seu *próprio domínio*. :smiley:


Além de banco de dados realtime e outras fantásticas funcionalidades para aplicações mobile e web, o Firebase também fornece um serviço de hospedagem para arquivos estáticos que todos *goodies* que sites modernos precisam:

- HTTPS automático;
- CDN com servidores no mundo inteiro;
- Deploy rápido através da Firebase CLI e;
- One-click rollbacks!

Acesse a [documentação de hospedagem no Firebase](https =//firebase.google.com/docs/hosting/) para mais detalhes.

## Ativar seu dominio

Agora você precisa apontar os registros de DNS do seu domínio para o nosso Firebase. Os registros específicos A ou CNAME DNS são exibidos no Console Firebase *uma vez certificado do seu domínio é ativado*. 

Uma vez que as alterações de DNS têm propagado, os usuários serão capazes de acessar seu site de hospedagem Firebase sobre o domínio conectado.

Dúvidas, consulte a [documentação oficial](https =//firebase.google.com/docs/hosting/custom-domain).


## Configurar o continuos deployment

Caso queira 

O que escrevi abaixo teve como base os seguintes tutoriais:

- https://ariya.io/2017/05/static-site-with-hugo-and-firebase
- https://gohugo.io/tutorials/hosting-on-bitbucket/

Você irá precisar gerar um `token` de acesso, e para isso será preciso instalar a CLI do Firebase em seu computador. 

1. Supondo que já tenha o [node.js] instalado, rode o seguinte no terminal para instalar a CLI: 

    ```bash
    npm install -g firebase-tools 
    ```

1. Depois que o processo de instalação terminal, será preciso fazer o login no Firebase. O comando abaixo irá abrir seu browser para fazer o login.

    ```bash
    firebase login
    ```

1. Agora será preciso criar um projeto, ou configurar um já pré-existante, para isso, crie ou vá ao diretório do seu projeto: 

    ```bash
    mkdir meu-projeto
    cd meu-projeto
    ```

1. Rode os comandos abaixo para iniciar e siga as instruções:

    ```bash
    firebase init
    ```

1. Agora você irá precisar criar um `token` e isso é bem simples, rode o comando:
 
    ```bash
    firebase login:ci
    ```

    O comando irá imprimir uma URL no terminal e pedirá para você visitar esta página. Feito isso, ele irá imprimir o `token` no terminal. 

1. Copie o `token` gerado no passo anterior e visite a página de configurações do seu repositório dentro do Bitbucket, que fica `Settings > Environment variables`. A url seria algo como: `https://bitbucket.org/USER/REPOSITORY/admin/addon/admin/pipelines/repository-variables.
    - Crie uma nova variável com o nome de `FIREBASE_TOKEN`, cole o que copiou no campo 'Value' e marque a opção 'Secured'. 

1. Crie um novo arquivo na raiz do seu projeto chamado bitbucket-pipelines.yml com o seguinte conteúdo:

```yaml
image: mhart/alpine-node:6.3

pipelines:
  branches:
    master:
      - step:
          script:
            - apk update && apk add openssl
            - wget https://github.com/spf13/hugo/releases/download/v0.21/hugo_0.21_Linux-64bit.tar.gz          
            - tar xf hugo_0.21_Linux-64bit.tar.gz && cp ./hugo /usr/bin
            - npm install -g firebase-tools
            - rm -rf public && hugo
            - firebase deploy --token $FIREBASE_TOKEN
```

Pronto! Agora toda vez que você fizer um commit no projeto, o Bitbucket irá rodar o Hugo para gerar os arquivos e fazer o *deploy* no Firebase automaticamente. Isso geralmente leva 1 minuto.

