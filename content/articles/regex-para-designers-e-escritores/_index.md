---
date: 2016-06-16
description: Primeiro post da série de tutoriais sobre expressões regulares e como elas podem te poupar muito tempo em tarefas tediosas na manipulação de textos
draft: true
featured: true
series:
- Regex para designers e escritores
slug: regex-para-designers-e-escritores
tags:
- regex
- expressões regulares
- produtividade
- tutorial
- ferramentas
- indesign
title: Porque todo designer, ou escritor, deveria conhecer as expressões regulares
---



Há tempos venho querendo escrever sobre o uso das **expressões regulares**, também conhecidas como [Regex](https://www.wikiwand.com/en/Regular_expression) ou [Grep](https://www.wikiwand.com/en/Grep), pois elas já me salvaram **incontáveis** horas de trabalho monótono pois são uma ferramenta poderosa para facilitar a vida de designers, escritores e programadores entre outras profissões que trabalham com conteúdo textual.

Se muitas vezes você precisou manipular quantidades de texto com algum tipo de repetição dentro do Indesign, teve que pegar conteúdo de um longo arquivo do Word enviado pela cliente e converter num HTML *clean*, precisou extrair trechos complexos entre outras peripécias, este *post* é para você! 

1. Não  serão necessários conhecimentos de programação, mas eu irei supor que você saiba se virar bem com um computador. 
2. As dicas serão dadas para o Mac OS, mas as Regex funcionam em qualquer sistema. E você mesmo que use Windows, não precisará instalar nada, pois há ferramentas online para testar suas expressões regulares.

## O que são expressões regulares?

As regex (*Regular Expressions*) são ...


    (!\[.+])(\()(Figura_\d.+)(\))

    <a href="https://www.example.com/images/$3">$0</a>


## Ferramentas

Há várias ferramentas online onde você pode aprender testando suas expressões regulares. Recomendo o http://regexr.com/ e são dele as capturas de tela que você verá nos exemplos abaixo. 

## Exemplos

### Alterando o protagonista aos 45 minutos do segundo tempo

Imagine que você está escrevendo uma história cujo protagonista originalmente se chamaria Sargento Oliveira, mas depois mudou de ideia por qualquer razão e resolveu alterar tanto seu posto como seu sobrenome, para Tenente Epaminondas. 

Essa mudança seria trivial, apenas fazer uma busca no seu editor de texto preferido e mandá-lo substituir tudo pelo novo nome, não é? Mas temos um problema, no afã de terminar o livro o mais rápido possível, você acabou usando variações como  Sgt. Oliveira, Sgto. Oliveira e "Sargento Oliveira**s**" entre outras. 

Vejamos alguns dos trechos de nossa fictícia história, que estariam espalhados em vários capítulos: 

> "Recém-promovido, o agora **Sgto. Oliveiras** mal podia acreditar na indisciplina da turma a sua frente. Seriam todos completos idiotas? Se perguntou ao martelar a mesa com seu punho."
>
> "Certo de que o inimigo avançava, **Oliveira** reuniu seus comandados na sala de guerra para apresentar sua nova estratégia…"
>
> "...estavam cercados, mas o **Sgt. Oliveira** era conhecido por ter escapado de cenários piores."
>
> "…estilhaços voaram como facas em brasa, e ao tentar se levantar, o **Sargento Oliveira** se contorceu e abafou um grito de horror, sua perna direita havia sido transformada em trapos de carne humana."

Como encontrar todas essas variações da forma mais rápida e segura possível, evitando retrabalho e erros na substituição? É aqui que entram as nossas salvadoras, as expressões regulares. 

O resultado final é esta regex:

```javascript
((Sargento|Sgt(o\.|\.)?)\s)?Oliveira[s]?
```

Em bom mineirês: *"Nóss sinhora, que trem complicado!"*

Calma. :wink:

Vamos construí-la passo a passo. A primeira coisa é encontrar todas as variações da patente do nosso protagonista. Sabemos que há "Sargento ou, Sgto. ou Sgt.". Isso daria a seguinte regex:

```
Sargento|Sgt(o\.|\.)
```

- `|` A barra vertical funciona como um "OU", ou isso ou aquilo.
- `\` A barra caída para a esquerda serve para 'escapar' um metacaractere, no caso o ponto final, transformando-o em um caractere normal. 


- `()`Os parênteses definem grupos de captura. Agrupam segmentos de forma que possamos manipulá-los separadamente. 


O jeito mais indicado de ler esta expressão é da esquerda para a direita, caracter por caracter. Então, seria algo assim:

Procure por "Sargento" OU por "Sgt" seguindo de um "o e um ponto final" ou seguido apenas um "ponto final".

Vamos ver como nossa regex se sai. 

![Resultado da expressão regular "Sargento|Sgt(o\.|\.)"](/img/posts/2016/regex/regex_tester_01.png)

Veja na imagem acima, tirada do http://regexr.com/,  os trechos marcados em azul. A expressão regular corresponde exatamente ao que queríamos:

```
Sgto.
Sgt.
Sargento
```

Agora, vamos montar a segunda parte da nossa regex, escreva no editor do site http://regexr.com/ o seguinte:

```
Oliveira[s]?
```

- `[]`Os colchetes definem uma lista de caracteres, onde qualquer um dos itens dentro da lista pode ser encontrado. No nosso exemplo, só usei os colchetes para facilitar a visualização. Como estamos buscando apenas uma letra, a "s", poderíamos removê-los, deixando nossa regex um pouco mais simples, `Oliveiras?`


- `?` O sinal de interrogação indica que o caractere, ou expressão (token), precedente a ele pode aparecer ou não na busca. Ou seja, estamos dizendo que queremos buscar por Oliveira ou Oliveira**s**.

Agora vamos juntar as duas partes e formar a nossa regex final. Até agora temos duas partes:

- Parte 1: `Sargento|Sgt(o\.|\.)`
- Parte 2: `Oliveira[s]?`

Mas **não** podemos simplesmente fazer isso:

```
Sargento|Sgt(o\.|\.)Oliveira[s]?
```

Por que? Retorne ao nosso texto de exemplo e veja que há um espaço separando as duas palavras, então precisamos cuidar disso. Vamos também agrupar as partes pois queremos achar o termo "Oliveira" sendo ou não precedido dos outros termos.

Então, teremos:

```javascript
((Sargento|Sgt(o\.|\.)?)\s)?Oliveira[s]?
```

- `\s` retorna um caractere espaço.

Marquei em negrito os novos caracteres da expressão. E testando-a no regexr.com, teremos o resultado mostrado na imagem a seguir:

![Resultado da expressão regular completa](/img/posts/2016/regex/regex_tester_02.png)

## Para aprender mais

### Livros

![Capa do livro Expressões Regulares](http://piazinho.com.br/ed5/capa-292.jpg)

[Expressões Regulares - Uma abordagem divertida](http://piazinho.com.br/) – Foi o primeiro livro em português sobre o regex, escrito de forma amigável e divertida, por Aurelio Marinho Jargas. Eu tinha a primeira edição, lançada em 2006, e a agora adquiri a quinta, lançada em 2016. 

[Expressões Regulares - Guia de Consulta Rápida](http://aurelio.net/regex/guia/)    – Guia para consultas rápidas, uma versão enxuta do conteúdo básico do livro Expressões Regulares - Uma abordagem divertida.

### Sites

Microsoft Word - [Finding and replacing characters using wildcards](http://word.mvps.org/faqs/general/usingwildcards.htm)

[IndesignSecrets](http://indesignsecrets.com/resources/grep) – Ótima lista de recursos sobre como usar Grep no Indesign.



