---
date: 2016-06-18T17:20:00+00:00
description: Segundo post da série de tutoriais sobre expressões regulares e como elas podem te poupar muito tempo em tarefas tediosas na manipulação de textos
draft: true
featured: false
series:
- Regex para designers e escritores
slug: usando-grep-no-indesign
tags:
- regex
- expressões regulares
- produtividade
- tutorial
- ferramentas
title: Como usar o GREP no Indesign
---

Segundo a [Wikipedia](http://en.wikipedia.org/wiki/Grep), o **grep** é um utilitário de linha de comando para a busca, em blocos de texto puro, por linhas que coincidem a uma expressão regular.

Não entendeu nada? Não "priemos cânico"! Com os exemplos que mostro abaixo, creio que ficará mais fácil você entender como usar as expressões regulares pode se tornar uma mão na roda e *aumentar sua produtividade*.

## Para aprender mais
Caso tenha ficado interessado em explorar o mundo de conhecimento sobre o assunto deste post, dê uma conferida em:

1. [Indesign Secrets: Grep](http://indesignsecrets.com/grep). Compilação de várias fontes de informação sobre o uso de GREP no Indesign.
2. [Expressões Regulares: Uma abordagem divertida - Aurelio Marinbho Jargas](http://www.piazinho.com.br/). Livro divertido e muito útil, com muitos exemplos de expressões regulares.