---
date: 2016-06-16T17:57:17-03:00
description: Aprenda a redimensionar imagens via terminal, sem perder nitidez e qualidade
tags:
- imagens
- produtividade
- ferramentas
title: Como redimensionar imagens em lote no Mac
---

Produzir um ebook para ser lido em qualquer tipo de aparelho é quase sempre um pé-no-saco devido às diferentes compatibilidades técnicas dos mesmos na intepretação dos códigos HTML, CSS e na *renderização* das imagens.

Peguemos o [Adobe Digital Editions](http://www.adobe.com/solutions/ebook/digital-editions.html) como exemplo; enquanto não posso reclamar dos outros softwares da Adobe, como o Photoshop, Indesign e Illustrator, o ADE é uma verdadeira budega. Caso você esteja começando no trabalho de produzir ebooks, sabia que o ADE será seu arqui-inimigo assim como o Internet Explorer 9- é para websites.

## O problema com o tamanho das imagens

O ADE mostra imagens com ``` max-width: 100% ``` no CSS de forma tosca, como se elas tivessem baixa resolução. E quando não há largura máxima configurada no CSS, e o tamanho da imagem for superior à dimensão padrão da janela do ADE (cerca de 600x800px), vemos a imagem sendo cortada.

Ou seja, uma verdadeira inhaca. A solução única passa a ser diminuir as imagens para 600px ou menos de largura e menos de 800px de altura na versão do epub que vai ser vendido em lojas que usam o *reader* baseado no ADE, como a Saraiva e manter as imagens com resolução maior no epub vendido nas outras.

## Como diminuir imagens em *batch* sem perda de qualidade

O processo de criação de um ebook é trabalhoso, por isso sempre tento otimizá-lo ao máximo. E como os ebooks que produzo têm muitas vezes centenas de imanges, seria burrice redimensioná-las uma a uma, deixe o computador fazer o trabalho sujo. *#lazyweb* :)

A forma mais rápida e eficaz que já encontrei, e acredite, já testei várias, desde:

- actions no Photoshop
- programas nativos do Mac como Preview
- programas de liha de comando como o sips (*scriptable image processing system*)

O **sips**, que é o comando usado dentro dos programas como o Preview, é muito bom, mas ao dimiuir as imagens, ele tende a gerar uma *perda de nitidez*, o que muitas vezes é inaceitável.

Continuando minhas pesquisas, encontrei o [**mogrify**](http://www.imagemagick.org/script/mogrify.php), programa de linha de comando que é parte da suíte de software [ImageMagick®](http://www.imagemagick.org/) é também bem poderoso, mas diferentemente do sips, não gera a perda de nitidez.

## Usando o mogrify

Usar comando `mogrify` é muito fácil, mas antes você precisa instalar a ImageMagick®, e o jeito mais fácil é usando o [*Homebrew*](http://brew.sh/):

    brew install imagemagick
    
Se estiver no Windows, instale usando o [Chocolately](https://chocolatey.org):

    choco install imagemagick

Uma vez instalada, navegue até o diretório das suas imagens e antes de mais nada, *faça um backup* das mesmas pois o comando abaixo é irá *SOBRESCREVER* os arquivos.

*Fez o BACKUP?* Então rode o comando no terminal no diretório das imagens:

    mogrify -filter lanczos2 -resize 600x800 *.png
    
No Windows, acrescente "magick" antes do comando. No exemplo abaixo, pra converter PNGs transparentes para fundo branco:

    magick mogrify -background white -flatten *.png

Essse comando irá usar o filtro `lanczos2` para garantir que a nitidez seja mantida na redução e irá diminuir todas as imagens .png do diretório para a largura máxima de 600px e a altura mázima de 800px, mas mantendo imagens menores que isso do jeito que estão.

Se quiser, você pode usar porcentagem também:

    mogrify -resize 50% nome-da-imagem.jpg

E também alterar o formato, convertendo todas as imagens PNG para JPEG:

    mogrify -format jpg *.png