---
title: "About"
description: "A few years ago, while visiting or, rather, rummaging about Notre-Dame, the author of this book found, in an obscure nook of one of the towers, the following word, engraved by hand upon the wall: —ANANKE."
featured_image: ''
comments: false
menu: main
---
{{< figure src="/images/Victor_Hugo-Hunchback.jpg" title="Illustration from Victor Hugo et son temps (1881)" >}}


Front-end Developer, Graphic Designer and Project Management Professional – PMP®. Current interests: Laravel, ProcessWire, Hugo and Latex.