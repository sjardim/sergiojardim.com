---
title: "Sobre"
description: "A few years ago, while visiting or, rather, rummaging about Notre-Dame, the author of this book found, in an obscure nook of one of the towers, the following word, engraved by hand upon the wall: —ANANKE."
featured_image: ''
comments: false
menu: main
---
{{< figure src="/images/Victor_Hugo-Hunchback.jpg" title="Illustration from Victor Hugo et son temps (1881)" >}}


Front-end Developer, Graphic Designer and Project Management Professional – PMP®. Current interests: Laravel, ProcessWire, Hugo and Latex.

## Sobre o site

Este site foi criado com o [Hugo](http://gohugo.io/), e tem seu [código fonte disponível no Bitbucket](https://bitbucket.org/sjardim/sergiojardim.com).

Ele está hospedado na plataforma [Firebase](https://firebase.google.com/docs/hosting/). 

[Neste artigo]({{< ref "firebase-hosting-setup.md" >}}) eu explico como você pode hospedar um site estático no Firebase.


## Filmes favoritos

Uma pequena lista dos meus top 10, cada um dos quais vistos várias vezes. Dados: [IMBD](imdb.com)

{{< movies >}}


