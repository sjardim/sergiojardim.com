# About
Source content of sergiojardim.com

Este site foi desenvolvido com o gerador de sites estáticos, [Hugo](http://gohugo.io/) e tem seu [código fonte disponível no Bitbucket](https://bitbucket.org/sjardim/sergiojardim.com).
